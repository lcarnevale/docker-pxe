FROM alpine
MAINTAINER Lorenzo Carnevale <lcarnevale@unime.it>

ADD http://archive.ubuntu.com/ubuntu/dists/trusty-updates/main/installer-amd64/current/images/netboot/netboot.tar.gz /netboot.tar.gz

RUN apk update && \
    apk --no-cache add \
    dnsmasq \
    tar
RUN mkdir /tftpboot && \
    tar -C /tftpboot -xvf /netboot.tar.gz && \
    rm /netboot.tar.gz && \
    chown -R nobody:nogroup /tftpboot

COPY dnsmasq.conf /etc/dnsmasq.conf

EXPOSE 69/udp

ENTRYPOINT ["/usr/sbin/dnsmasq", "-k"]