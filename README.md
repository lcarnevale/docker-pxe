# Alpine PXE Server on Docker
This repository provides a docker container for serving Ubuntu netboot image for PXE-boot. The container uses Alpine Linux and a DNS Server already existing on your network.

## How to Build it
```bash
docker build -t local/pxe-alpine .
```

## How to Run it
```bash
docker run -d -p 69:69/udp --rm --privileged --net host local/pxe-alpine
```

## Credits
This repository is based on the [thereapsz](https://github.com/thereapsz) repository, which you can find [here](https://github.com/thereapsz/alpine-pxe).
